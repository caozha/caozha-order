# caozha-order 广告竞价页订单管理系统

caozha-order是一个通用的广告（推广投放）竞价页订单管理系统，基于开源的caozha-admin开发，支持订单管理、订单回收站、产品管理、批量上传订单、批量导出订单（支持导出格式：.xls，.xlsx，.csv）、检测订单重复、竞价页的下单表单调用、客户下单时给管理员发邮件（短信）提醒等功能，内置灵活的查看订单权限设置机制。

## 重要说明

## 本项目已经迁移，请您到以下的新网址去下载和使用：


 **Gitee（国内仓库）：** https://gitee.com/dengzhenhua/caozha-order

 **GitHub（国外仓库）：** https://github.com/dengcao/caozha-order

